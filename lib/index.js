'use strict';

const Bossy    = require('@hapi/bossy');
const Knex     = require('knex');
const Fs       = require('fs');
const Readline = require('readline');
const Ora      = require('ora');

const internals = {
    definition : {
        file     : {
            description : 'File to import',
            alias       : 'f',
            type        : 'string',
            require     : true
        },
        database : {
            description : 'Database to import to',
            alias       : 'd',
            type        : 'string',
            default     : 'films'
        },
        table    : {
            description : 'Table',
            alias       : 't',
            type        : 'string',
            default     : 'title'
        },
        host     : {
            description : 'Database host',
            alias       : 'h',
            type        : 'string',
            default     : '0.0.0.0'
        },
        client   : {
            description : 'Database type',
            alias       : 'c',
            type        : 'string',
            valid       : ['pg', 'mysql'],
            default     : 'pg'
        },
        port     : {
            description : 'Database port',
            alias       : 'P',
            type        : 'string',
            default     : '5432'
        },
        username : {
            description : 'Database username',
            alias       : 'u',
            type        : 'string',
            require     : true
        },
        password : {
            description : 'Database password',
            alias       : 'p',
            type        : 'string'
        }
    }
};

const args = Bossy.parse(internals.definition);

if (args instanceof Error) {

    console.error(args.message);
    return;
}

const run = async () => {

    const spinner = Ora('Connecting to Database').start();

    const knex = Knex({
        client     : args.client,
        connection : {
            host     : args.host,
            user     : args.username,
            password : args.password,
            database : args.database
        }
    });

    const res = await knex.schema.hasTable(args.table);

    if (!res) {
        spinner.text = 'No Table found, creating it...';

        await knex.schema.createTable(args.table, (table) => {

            table.increments('id');
            table.string('type');
            table.string('primaryTitle');
            table.string('originalTitle');
            table.boolean('isAdult');
            table.integer('startYear');
            table.integer('endYear');
            table.integer('runtimeMinutes');
            table.jsonb('genres');
        });
    }

    let i = 0;

    knex.truncate(args.table);

    const rl = Readline.createInterface({
        input : Fs.createReadStream(args.file)
    });

    const getLineGen = async function* () {
        for await (const line of rl) {
            yield line;
        }
    };

    for await (const line of getLineGen()) {

        if (i !== 0) {

            spinner.text = `Importing from file (${ i })`;

            const [, titleType, primaryType, originalType, isAdult, startYear, endYear, runtimeMinutes, genres] = line.split('\t');

            await knex.insert({
                type           : titleType,
                primaryTitle   : primaryType,
                originalTitle  : originalType,
                isAdult        : isAdult !== '0',
                startYear      : startYear === '\\N' ? null : startYear,
                endYear        : endYear === '\\N' ? null : endYear,
                runtimeMinutes : runtimeMinutes === '\\N' ? 0 : runtimeMinutes,
                genres         : JSON.stringify(genres ? genres.split(',') : [])
            }, 'id').into(args.table);
        }

        i++;
    }

    process.exit();
};

process.on('unhandledRejection', (error) => {
    // Will print "unhandledRejection err is not defined"
    console.log('');
    console.error(error.message);
    process.exit();
});

run();
