# IMBD Titles parser

Extracts data from IMBD .tsv title files to import them into a sql database (mysql/postgres)

## Usage

NODE >12

```
node lib/index.js --file ./title.basics.tsv -u hapi -p hapi -h 0.0.0.0
```


## Options

***`file`*** or ***`f`*** (required): Path to the .tsv file from IMBD   
***`database`*** or ***`d`***: Database name   
***`table`*** or ***`t`***:  Database table   
***`host`*** or ***`h`***:  Database host   
***`client`*** or ***`c`*** (default `ps`):  Database client (`mysql` or `ps`)   
***`port`*** or ***`P`*** (default `5432`): Database port  
***`username`*** or ***`u`*** (required): Database username  
***`password`*** or ***`p`***: Database password  
